import React from "react";
import { ViewStyle } from "react-native";

export type ToggleSwitchProps = {
  isOn: boolean;
  label?: string;
  onColor?: string;
  offColor?: string;
  size?: string;
  style?: ViewStyle;
  labelStyle?: object | number;
  thumbOnStyle?: object | number;
  thumbOffStyle?: object | number;
  trackOnStyle?: object | number;
  trackOffStyle?: object | number;
  onToggle?: (isOn: boolean) => any;
  icon?: object;
  disabled?: boolean;
  animationSpeed?: number;
  useNativeDriver?: boolean;
  circleColor?: string;
};
export default class ToggleSwitch extends React.Component<ToggleSwitchProps> {}
